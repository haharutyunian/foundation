<footer class="pt-2 pl-1 my-md-5 pt-md-2">
    <div class="row">
        <div class="col-md-2">
           <div>+37455337099</div>
           <div>+37477337099</div>
        </div>
        <div class="col-md-4">
            <div><a href="mailto:vahe@meliksetyan.org" target="_self" class="email-address">vahe@meliksetyan.org</a></div>
            <div><p class="address" style="font-size:16px">{!! trans('contact.address') !!}</p></div>
        </div>
        <div class="col-md-4">
            <span style="text-decoration:underline">
                <a class="copyright" href="{{'/files/am-kanonadrutyun.pdf'}}" target="_blank">
                    © {!! trans('contact.copyright') !!}
                </a>
            </span>
        </div>
        <div class="col-md-2">
            <div class="social-links">
                <a href="https://www.facebook.com/meliksetyan.org" target="_blank">
                    <div class="facebook"></div>
                </a>
                <a href="https://www.linkedin.com/company/meliksetyan/" target="_blank">
                    <div class="linkedin"></div>
                </a>
            </div>
        </div>
    </div>
</footer>
