<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
@yield('title')
<base href="{{config('app.url')}}">
<meta property="og:url" content="https://www.meliksetyan.org">
<meta property="og:site_name" content="VaheMeliksetyan Fund">
<meta property="og:type" content="website">
<link rel="canonical" href="VaheMeliksetyan Fund">



